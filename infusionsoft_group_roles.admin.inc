<?php
/**
 * @file
 * Admin Settings for Infusionsoft Group (Tags) and Drupal roles mapping.
 */

/**
 * Settings Form to map Drupal roles with infusionsoft tags
 */
function infusionsoft_group_roles_settings_form() {
  if (!infusionsoft_check_settings()) {
    return t('Please check infusionsoft settings and make sure website is connected to infusionsoft');
  }
  $group_categories = infusionsoft_get_group_categories();
  if (!sizeof($group_categories)) {
    return t('No group categories found in infusionsoft.');
  }
  $group_categories = array('' => t('select')) + $group_categories;
  $form = array();
  $group_category_id = variable_get('infusionsoft_group_roles_group_category', 0);
  $group_category_name = variable_get('infusionsoft_group_roles_group_category_name', '');
  if ($group_category_id) {
    $form['change'] = array(
      '#value' => t('Change infusionsoft group category'),
      '#type' => 'submit',
    );
  }
  if (!$group_category_id) {
    $form['infusionsoft_group_roles_group_category'] = array(
      '#type' => 'select',
      '#default_value' => variable_get('infusionsoft_group_roles_group_category', 0),
      '#options' => $group_categories,
      '#title' => t('Select Infusionsoft group category'),
      '#required' => TRUE,
      '#description' => t('Fetch tags from infusionsoft to map with drupal roles'),
    );
  }
  else {
    $form['category_selected'] = array(
      '#type' => 'markup',
      '#prefix' => '<div>',
      '#suffix' => '</div>',
      '#markup' => check_plain(t('Map infusionsoft group category !categoryname tags with drupal roles below' , array('!categoryname' => $group_category_name))),
    );
  }
  if ($group_category_id) {
    $form['infusionsoft_group_roles'] = array(
      '#type' => 'fieldset',
      '#title' => t('Drupal Role Mapping with Infusionsoft tags'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['infusionsoft_group_roles']['infusionsoft_group_roles_update_on_login'] = array(
      '#type' => 'checkbox',
      '#default_value' => variable_get('infusionsoft_group_roles_update_on_login', 0),
      '#title' => t('Update roles on login'),
      '#description' => t('Check this if you want to sync Drupal roles with infusionsoft tags everytime user logins.'),
    );
    $groups = infusionsoft_group_roles_get_groups_by_category($group_category_id);
    $roles = user_roles();
    unset($roles[1] , $roles[2]);
    $groups = array('' => t('--Do not map this role--')) + $groups;
    $result = db_query("SELECT tid, rid FROM {infusionsoft_group_roles_linker}");
    $mapping =  array();
    foreach ($result as $record) {
      $mapping[$record->rid] = $record->tid;
    }
    foreach ($roles as $rid => $role_name) {
      $form['infusionsoft_group_roles'][$rid] = array(
        '#type' => 'select',
        '#default_value' => isset($mapping[$rid]) ? $mapping[$rid] : '',
        '#options' => $groups,
        '#title' => check_plain(t('Map Drupal role "!role" with infusionsoft role' , array('!role' => $role_name))),
      );
    }
    $form['roles'] = array('#type' => 'value', '#value' => $roles);
    $form['infusionsoft_group_roles']['savemapping'] = array(
      '#value' => t('Map Tags with Drupal Roles'),
      '#type' => 'submit',
    );
  }
  else {
    $form['savetag'] = array(
      '#value' => t('Fetch Tags'),
      '#type' => 'submit',
    );
  }
  return $form;
}


function infusionsoft_group_roles_settings_form_submit($form, &$form_state) {
  if (isset($form_state['values']['change']) && $form_state['values']['change'] == $form_state['clicked_button']['#value']) {
    db_delete('infusionsoft_group_roles_linker')->execute();
    variable_del('infusionsoft_group_roles_group_category');
    variable_del('infusionsoft_group_roles_group_category_name');
  }
  if (isset($form_state['values']['savetag']) && $form_state['values']['savetag'] == $form_state['clicked_button']['#value']) {
    variable_set('infusionsoft_group_roles_group_category', $form_state['values']['infusionsoft_group_roles_group_category']);
    $group_categories = infusionsoft_get_group_categories();
    variable_set('infusionsoft_group_roles_group_category_name', $group_categories[$form_state['values']['infusionsoft_group_roles_group_category']]);
  }
  if (isset($form_state['values']['savemapping']) && $form_state['values']['savemapping'] == $form_state['clicked_button']['#value']) {
    variable_set('infusionsoft_group_roles_update_on_login', $form_state['values']['infusionsoft_group_roles_group_category']);
    db_delete('infusionsoft_group_roles_linker')->execute();
    $roles = $form_state['values']['roles'];
    foreach ($roles as $rid =>  $role_name) {
      if (is_numeric($form_state['values'][$rid])) {
        $insert_rows[] = array('tid' => $form_state['values'][$rid] , 'rid' => $rid);
      }
    }
    $query = db_insert('infusionsoft_group_roles_linker')->fields(array('tid', 'rid'));
    foreach ($insert_rows as $record) {
      $query->values($record);
    }
    $query->execute();
  }
}

