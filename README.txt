
Infusionsoft Group Roles
========================

This module allows to add or remove Drupal roles automatically depending upon Infusionsoft tags(groups) added  to user in infusionsoft account

1. In Infusionsoft create a tag category called “Roles”. you can name it “Roles” or anything else.
2. Add Drupal roles as tags in infusionsoft  under “Roles” category. Tag names can be same as Drupal role name or different.
3. Goto admin/config/services/infusionsoft-roles to map Drupal roles with infusionsoft tags
4. By default when user/<uid/infusionsoft page is viewed, automatic role assignation is done depending upon the infusionsoft Role Category tags User has.
5. We can also execute Auto role assignation on user login by enabling it from admin settings page admin/config/services/infusionsoft-roles
6. If User roles are updated from Drupal admin area. Corresponding tags will be updated in infusionsoft. 
7. Drupal role which is not mapped to any Infusionsoft tag does not get added or removed by this module to any user.