<?php

function infusionsoft_group_roles_init() {
  if (arg(0) == 'user' && is_numeric(arg(1)) && arg(2) == "infusionsoft") {
    infusionsoft_group_roles_sync_roles(user_load(arg(1)));
  }
}

/**
 * Implements hook_menu().
 */
function infusionsoft_group_roles_menu() {
  $items = array();
  $items['admin/config/services/infusionsoft-roles'] = array(
    'title' => 'Infusionsoft Group Roles Settings',
    'description' => 'Map infusionsoft tags with Drupal roles.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('infusionsoft_group_roles_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'infusionsoft_group_roles.admin.inc',
  );
  return $items;
}



/**
 * Get a list of all groups (tags) of a group category.
 *
 * @param string $pattern
 *   Id to match against GroupCategoryId field.
 */
function infusionsoft_group_roles_get_groups_by_category($id = '%') {
  $service = wsclient_service_load('infusionsoft_api');
  $return = $service->{"DataService.query"}('ContactGroup', 1000, 0, array("GroupCategoryId" => $id), array('Id', 'GroupName', 'GroupCategoryId', 'GroupDescription'));

  $categories = infusionsoft_get_group_categories();
  $out = array();
  if ($return) {
    foreach ($return as $ret) {
      if (isset($ret['GroupName'])) {
        $out[$ret['Id']] = $ret['GroupName'];
      }
    }
  }

  return $out;
}

/**
 * Check Infustionsoft Group tags and reassign Drupal roles depending upon tags.
 */
function infusionsoft_group_roles_user_login(&$edit, $account) {
  if (variable_get('infusionsoft_group_roles_update_on_login', 0)) {
    infusionsoft_group_roles_user_sync_roles($account);
  }
}


function infusionsoft_group_roles_user_update(&$edit, $account, $category) {
  if (!infusionsoft_check_settings()) {
    return FALSE;
  }
  $infusionsoft_id = infusionsoft_contact_id($account, FALSE);
  if (!$infusionsoft_id) {
    return FALSE;
  }
  $roles = $account->roles;
  $result = db_query("SELECT tid, rid FROM {infusionsoft_group_roles_linker}");
  foreach ($result as $record) {
    $mapping[$record->rid] = $record->tid;
  }
  foreach ($roles as $rid => $role_name) {
    if (isset($mapping[$rid])) {
      infusionsoft_user_op("add", "group", $infusionsoft_id, $mapping[$rid]);
      unset($mapping[$rid]);
    }
  }
  foreach ($mapping as $rid => $tid) {
    infusionsoft_user_op("remove", "group", $infusionsoft_id, $tid);
  }
}

function infusionsoft_group_roles_sync_roles($account) {
  if (!infusionsoft_check_settings()) {
    return FALSE;
  }
  $infusionsoft_id = infusionsoft_contact_id($account, FALSE);
  if (!$infusionsoft_id) {
    $infusionsoft_id = infusionsoft_user_to_contact($account);
  }
  $group_category_id = variable_get('infusionsoft_group_roles_group_category', 0);
  if (!$infusionsoft_id && !$group_category_id) {
    return;
  }
  $user_groups = infusionsoft_group_contact_options($infusionsoft_id);
  $groups_by_category = infusionsoft_group_roles_get_groups_by_category($group_category_id);
  $result = db_query("SELECT tid, rid FROM {infusionsoft_group_roles_linker}");
  foreach ($result as $record) {
    $mapping[$record->tid] = $record->rid;
  }
  $user_roles = $account->roles;
  $roles = user_roles(TRUE);
  foreach ($user_groups as $user_group) {
    if (array_key_exists($user_group['GroupId'] , $groups_by_category)) {
      $user_roles[$mapping[$user_group['GroupId']]] = $roles[$mapping[$user_group['GroupId']]];
      unset($groups_by_category[$user_group['GroupId']]);
    }
  }
  foreach ($groups_by_category as $group_category_id => $group_category_name) {
    unset($user_roles[$mapping[$group_category_id]]);
  }
  user_save($account, array('roles' => $user_roles));
}


function infusionsoft_group_roles_form_infusionsoft_user_contact_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['groups'])) {
    foreach ($form['groups'] as $key => $group) {
      if (is_numeric($key) && $group['#type'] == "submit") {
        $form['groups'][$key]['#submit'][] = 'infusionsoft_group_roles_user_contact_form_submit';
      }
    }
  }
  if (isset($form['available_groups'])) {
    foreach ($form['available_groups'] as $key => $group) {
      if (is_numeric($key) && $group['#type'] == "submit") {
        $form['available_groups'][$key]['#submit'][] = 'infusionsoft_group_roles_user_contact_form_submit';
      }
    }
  }
}

function infusionsoft_group_roles_user_contact_form_submit($form, &$form_state) {
  $action = $form_state['triggering_element']['#name'];
  if (strpos($action, "remove_group_") !== FALSE) {
    $group_id = str_replace("remove_group_", "", $action);
    $action = "remove_group";
  }
  if (strpos($action, "add_group_") !== FALSE) {
    $group_id = str_replace("add_group_", "", $action);
    $action = "add_group";
  }
  if ($action == "remove_group" || $action == "add_group") {
    infusionsoft_group_roles_sync_roles($form_state['user']);
  }
}